import { Routes, Route } from 'react-router-dom';
import { AuthProvider } from './context';
import routes from './config/routes';
import './App.scss';

const App = () => {
    return (
        <AuthProvider>
            <div className="app">
                <h1>Working!</h1>
                <Routes>
                    {routes.map(({ path, element }) => {
                        return <Route key={path} path={path} element={element} />;
                    })}
                </Routes>
            </div>
        </AuthProvider>
    );
};

export default App;
