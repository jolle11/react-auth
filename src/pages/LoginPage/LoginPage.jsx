import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { loginUser, useAuthDispatch, useAuthState } from '../../context';
import styles from './LoginPage.module.css';

// We set an initial state that will bet setted to the form by default
const INITIAL_STATE = {
    email: '',
    password: '',
};

// We start creating the LoginPage
const LoginPage = () => {
    // Using useState, we create the form and set it to the initial state we created before
    const [form, setForm] = useState(INITIAL_STATE);
    // We use the dispatch, the navigate and the state which we imported form the context
    const dispatch = useAuthDispatch();
    const navigate = useNavigate();
    const state = useAuthState();
    // We create the function that will change the inputs when we type in the
    // It receives the event (in this case the key we press)
    const inputChange = (event) => {
        // We destructure the event.target into the id it has (email or password) and the value (what we are typing)
        const { id, value } = event.target;
        // We set the form creating a copy of it and setting the value we just modified to the new one
        setForm({ ...form, [id]: value });
    };
    // We create the submitForm function, which is going to be an async function
    // It is an async function because after the event, in this case the click of the button,
    // has to wait for the data we passed in to be checked with the loginUser function.
    // If the response is ok (dispatch shoots the action using the data from the form),
    // Option 1: if the user does not exist, we go straight to the catch and show the error
    // Option 2: if the user exists, we navigate to the dashboard page
    const submitForm = async (event) => {
        // We prevent the default behaviour of the form
        event.preventDefault();
        // We use a try catch
        try {
            // We check if the user exists in our DB using the loginUser function
            const response = await loginUser(dispatch, form);
            // If we don't receive any user, we exit the try and go to the catch
            if (!response.user) return;
            // If we receive a user, we navigate to the dashboard page
            navigate('/dashboard');
        } catch (error) {
            // Given the case that we find no use, the catch will display the correct error for the case
            console.log(error);
        }
    };

    return (
        <div className={styles.container}>
            <div className={styles.formContainer}>
                <h1>Login Page</h1>
                <form onSubmit={submitForm}>
                    <div className={styles.loginForm}>
                        <div className={styles.loginFormItem}>
                            <label htmlFor="email">Username</label>
                            <input type="text" id="email" onChange={inputChange} value={form.email} />
                        </div>
                        <div className={styles.loginFormItem}>
                            <label htmlFor="password">Password</label>
                            <input type="password" id="password" onChange={inputChange} value={form.password} />
                        </div>
                    </div>
                    <button>Login</button>
                </form>
            </div>
            {state.error && <p style={{ color: 'red' }}>{state.error}</p>}
        </div>
    );
};

export default LoginPage;
