import LoginPage from './LoginPage/LoginPage';
import DashboardPage from './DashboardPage/DashboardPage';
import NotFoundPage from './NotFoundPage/NotFoundPage';

export { LoginPage, DashboardPage, NotFoundPage };
