// We import the hooks we need
import { createContext, useContext, useReducer } from 'react';
// We import the initial state and the authReducer
import { INITIAL_STATE, authReducer } from './reducer';

// We create the context for the state and the dispatch
const AuthStateContext = createContext();
const AuthDispatchContext = createContext();

// We create the function for the useAuthState context
const useAuthState = () => {
    // If there's a context, we use it
    const context = useContext(AuthStateContext);
    // If not, we throw an error
    if (context === undefined) {
        throw new Error('useAuthState has to be used inside an <AuthProvider>');
    }
    // We return the context
    return context;
};

// We create the function for the useAuthDispatch context
const useAuthDispatch = () => {
    // If there's a context, we use it
    const context = useContext(AuthDispatchContext);
    // If not, we throw an error
    if (context === undefined) {
        throw new Error('useAuthDispatch has to be used inside an <AuthProvider>');
    }
    // We return the context
    return context;
};

// We create the AuthProvider will receive the props (the App itself)
const AuthProvider = (props) => {
    // The next const will bring use the user and the dispatch action
    // Inside the const we use the useReducer hook to start the authReducer function,
    // and pass in the INITIAL_STATE from the reducer
    const [user, dispatch] = useReducer(authReducer, INITIAL_STATE);
    // We return the next "component" with the contexts combined
    return (
        // The AuthStateContext which will receive a value, in this case the user
        <AuthStateContext.Provider value={user}>
            {/* The AuthDispatchContext receives the dispatch and when the login is ok it shows the page with props.children
                We can see this in the App.jsx */}
            <AuthDispatchContext.Provider value={dispatch}>{props.children}</AuthDispatchContext.Provider>
        </AuthStateContext.Provider>
    );
};

// We export the funcitons for the context
export { useAuthState, useAuthDispatch, AuthProvider };
