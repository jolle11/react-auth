// We import all(*) the actions from the actions file
import * as actions from './actions';

// We set an initial state for the authReducer
const INITIAL_STATE = {
    user: '',
    token: '',
    loading: false,
    error: '',
};

// We create the authReducer and assign the initial state to the state and use action to handle the different actions we can perform
const authReducer = (state = INITIAL_STATE, action) => {
    // We specify that the action has a type and a payload
    const { type, payload } = action;
    // We start with a switch to handle the different actions and we specify the we are going to pass the type
    switch (type) {
        // Case one: action login request
        case actions.LOGIN_REQUEST: {
            // We return the a copy of the state and we change the loading property to true
            return { ...state, loading: true };
        }
        // Case two: action login ok
        case actions.LOGIN_OK: {
            // We return a copy of the state and we set the user, the token and the loading
            return { ...state, user: payload.user, token: payload.token, loading: false };
        }
        // Case three: action login error
        case actions.LOGIN_ERROR: {
            // We return a copy of the state and set the loading and the error
            return { ...state, loading: false, error: payload.error };
        }
        // We have to set a default case for the action which returns the state
        default:
            return state;
    }
};
// We export the INITIAL_STATE and the authReducer
export { INITIAL_STATE, authReducer };
