// First of all we create the actions
// When the user requests the login
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
// When the credentials are correct
export const LOGIN_OK = 'LOGIN_OK';
// When the credentials are incorrect or something went wrong
export const LOGIN_ERROR = 'LOGIN_ERROR';

// We create a const for the API url
const URL = 'https://secret-hamlet-03431.herokuapp.com';

// We create the function for the login and we pass in the dispatch, that is going to run the action and the loginData so it can authenticate the use
export const loginUser = async (dispatch, loginData) => {
    // Using try catch
    try {
        console.log('loginUser function', loginUser);
        // We dispatch the first action type which is for the request to login
        dispatch({
            // We define the action type
            type: LOGIN_REQUEST,
        });
        // Then we make the call to the API using fetch and specifying the url and what is going to be (endpoint)
        const req = await fetch(URL + '/login', {
            // We define the method type, the headers and the body (which is going to be the login data stringified)
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(loginData),
        });
        // We pass the data we received to a const named data
        const data = await req.json();
        console.log('data from server', data);
        // Using an if conditional, we check if the user exists comparing the data we passed with the data we received
        if (data.user) {
            // If the data exists, we dispatch the action for the login ok, passing in the type of action and the payload
            dispatch({ type: LOGIN_OK, payload: data });
            // We set the user inside the localStorage for future logins
            localStorage.setItem('user', JSON.stringify(data));
            // We return the data of the user (in this case user and password for example)
            return data;
        }
        // In the else statement we dispatch the login error action and in the payload we display the error message we get inside the data we received
        else {
            dispatch({ type: LOGIN_ERROR, payload: data.errors[0] });
            return;
        }
    } catch (error) {
        // We use the catch to display any given error we can have if something with the login goes the wrong way
        dispatch({ type: LOGIN_ERROR, payload: error });
        return;
    }
};
// We export the logout function
export const logoutuser = () => {};
