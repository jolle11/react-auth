// We import the pages from the pages folder
import { LoginPage, DashboardPage, NotFoundPage } from '../pages';
// We create a const that will contain the routes for every page
const routes = [
    // We use / for the home page or the page by default
    { path: '/', element: <LoginPage /> },
    // We use /dashboard to go to the dashboard page
    { path: '/dashboard', element: <DashboardPage /> },
    // We use /* to show the not found page when the address is something that's not specified by us
    { path: '/*', element: <NotFoundPage /> },
];
// We export the const routes which we are going to use in our App.jsx
export default routes;
